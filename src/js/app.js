import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebP();

const menuBtn = document.querySelector('.menu-drop-down');

menuBtn.addEventListener('click', (ev) => {

    if(ev.target.closest('.menu-btn')){
        menuBtn.classList.add('active')
    }
    if(ev.target.closest('.close')) {
        menuBtn.classList.remove('active')
    }
})